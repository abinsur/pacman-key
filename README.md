<div align="center">

[<img src="https://i.imgur.com/UjnRFbR.png" width="500">](https://archlinux.org/)

</div>

# [pacman-key](https://wiki.archlinux.org/title/Pacman/Package_signing)

`# rm -fr /etc/pacman.d/gnupg`  
*remover as chaves antigas*

`# pacman-key --init`  
*iniciar novo chaveiro*

`# pacman-key --populate archlinux`   
*verificar chaves mestras*

`# pacman-key --refresh-keys --updatedb`   
*recarregar e atualizar*

`# pacman -Sy --overwrite \* archlinux-keyring`   
*instalar um novo chaveiro*

`# pacman -Syu`   
*atualizar o sistema*



<div align="center">

Copyright © 2002-2023 Judd Vinet, Aaron Griffin and Levente Polyák.

The [Arch Linux](https://archlinux.org/) name and logo are recognized trademarks. Some rights reserved.

[Linux®](https://www.kernel.org/) is a registered trademark of Linus Torvalds.

</div>